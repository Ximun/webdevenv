# My Webdev Environment using Docker, Php, MariaDB, phpmyadmin et wp-cli for Wordpress websites

## Dependencies

* make
* docker
* docker-compose

## How to use it

### Docker

Put your Wordpress php files into the app/ folder. Run `docker-compose up -d` or `make docker-up`. This will start a web server container using ubuntu and php 7.2, a MariaDB database 10.0.36 and a phpmyadmin container.

You can access to your website at `http://localhost:8080`. You can access to your phpmyadmin administration at `http://localhost:8081`. User and password are both `root`

For your application, your database host is `db`. You have to create a database with phpmyadmin first, by default your mariadb server has no database.

### Make

A Makefile can use it to deploy or import your website on remote host, assuming you have a SSH access authentified by keys on it. First, enter the options:

* `path`: your /path/to/website on remote host.
* `ssh`: your username @ hostname connexion (assuming using port 22)
* `domain_local`: your local app access domain (default http://localhost:8080)
* `domain_remote`: your public app access domain on internet

If you want to exclude some files or folders while you are deploying or importing your website files, write them into `deploy.exclude` and `import.excludes`. Be careful with sensitive data.

You can now access to different make command:

* `make help`: display commands
* `make docker-up`: start docker containers
* `make docker-down`: stop app running docker containers
* `make deploy`: deploy your app files on remote server
* `make import`: import your app files on local folder
* `make dbdeploy`: deploy your database on remote server
* `make dbimport`: import your database on local instance
* `make dbcheck`: check your local database

**Important: These database Make commands assume that your are running a Wordpress app**
