.PHONY: dev docker-up docker-down deploy dbcheck dbdeploy import dbimport help update

#options='-p 55555'
path=~/example/
ssh=user@host.com
domain_local=http://localhost:8080
domain_remote=http://www.example.com

help: ## Affiche cette aide
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-20s\033[0m %s\n", $$1, $$2}'

docker-up: ## Lance un environnement docker php + Apache + Mariadb
	docker-compose up -d

docker-down: ## Stoppe les containers docker
	docker-compose stop apache phpmyadmin db

deploy: ## Déploie une nouvelle version de l'application
	rsync -av ./app/ $(ssh):$(path) --exclude-from=./deploy.excludes

import: ## Importer le site depuis l'hôte
	rsync -avzhP $(ssh):$(path) ./app/ --exclude-from=./import.excludes
	chmod 755 app/

dbimport: ## Importer la base de données
	ssh $(ssh) "cd $(path); wp db export dump.sql;"
	rsync -av $(ssh):$(path)dump.sql ./app/dump.sql
	ssh $(ssh) "cd $(path); rm dump.sql;"
	docker-compose exec apache sh -c "wp db import /app/dump.sql --allow-root --path=/app; wp search-replace '$(domain_remote)' '$(domain_local)' --allow-root --path=/app;"
	rm ./app/dump.sql

dbcheck: ## Analyse la base de données Wordpress
	docker-compose exec apache sh -c 'wp db check --allow-root --path=/app'

dbdeploy: ## Envoie les données sur le serveur
	docker-compose exec apache sh -c "wp db export --add-drop-table dump.sql --path=./app --allow-root"
	docker cp $(shell docker-compose ps -q apache):dump.sql ./
	rsync -av dump.sql $(ssh):$(path)dump.sql
	ssh $(ssh) "cd $(path); wp db import dump.sql; wp search-replace '$(domain_local)' '$(domain_remote)' --allow-root; rm dump.sql"
	docker-compose exec apache sh -c "rm dump.sql"
